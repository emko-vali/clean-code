	public static void removeTourNameAndRemark(FileModel fileModel) {
		if (fileModel != null && !StringUtils.isEmpty(fileModel.getFileContent())) {
			FileType tifFIleDescriptor = FileType.TifFile;
			final String[] rows = fileModel.getFileContent().replace("\r", "").split("\n", -1);
			boolean fileChanged = false;

			for (int i = (rows.length - 1); i > 0; i--) {
				final String[] cellData = rows[i].split(tifFIleDescriptor.getColDelimiter(),-1);
				//remove receiver name from tour scans
				if (cellData.length == TIF_COLUMN_COUNT && cellData[TifColumn.eventType.getValue()].trim().equals(TOUR_EVENT_NUMBER)) {
					cellData[TifColumn.shipperOrReceiverName.getValue()] = "";
					cellData[TifColumn.remark.getValue()] = "";
					String newRow = Utils.joinString(tifFIleDescriptor.getColDelimiter(), cellData);
					log.info("Removed tourReceiver from row: " + rows[i] + " to: " + newRow);
					rows[i] = newRow;
					fileChanged = true;
				}
			}
				// do the changes in the file if needed
				if (fileChanged) {
					fileModel.setFileContent(Utils.joinString(tifFIleDescriptor.getNewlineDelimiter(), rows) + tifFIleDescriptor.getNewlineDelimiter());
					log.info("Tour receivers removed: " + fileModel.getFileName());
				}
		}
	}
	
	public static void removeNameAndRemarkForTourLines(TifFileV3 tifFileV3) {
		List<TifFileLine> tourLines = tifFileV3.getLinesByEventType(Integer.parseInt(TOUR_EVENT_NUMBER));
		tourLines.stream()
			.filter(line -> line.getColumnCount() == TIF_COLUMN_COUNT)
			.forEach(line -> {
				line.setRemark("");
				line.setShipperOrReceiverName("");
			});
		
		if (tourLines.size() > 0) {
			log.info("Tour receivers and remarks removed from {} lines in file {}", tourLines.size(), tifFileV3.getFileName());
		}
	}