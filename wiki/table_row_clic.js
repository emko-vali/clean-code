// Metoda emituje rowClicked aj s prazdnym id
// Chyba sa mozno skryva v takejto metode, vieme v rychlosti povedat co robi aby sme mohli patrat kde moze byt chyba?

onClickRow(row) {
  if (this.editable) {
    if (this.selectable) {

      if ( ! this.multiselectable) {
        this.highlightedRow = row;
      } else {
        // check filter
        if (  this.multiselectableFilter.length  !== 0 ) {
          const failedMemeters: string[] = [];
          if (  this.highlightedRows.length !== 0 ) {

            // check  firs entry set filter value
            this.multiselectableFilter.forEach(( element, index) => {

              // check if   elemtn id data is undefined
              if ( typeof this.multiselectableFilterData[index]  !== 'undefined' ) {
                // compare with value in Data
                if ( Array.isArray(this.multiselectableFilterData[index]) ) {
                  if ( this.multiselectableFilterData[index].indexOf(row[element]) < 0  ) {
                    // Data contains array of values
                    failedMemeters.push(element);
                  }
                } else {
                  if ( this.multiselectableFilterData[index] !== row[element] ) {
                    failedMemeters.push(element);
                  }
                }
              } else // compare with 1st element
                if ( this.highlightedRows[0][element] !== row[element]  ) {
                  failedMemeters.push(element);
                }
            });
          } else {
            // no highlitget row
            this.multiselectableFilter.forEach(( element, index) => {
              if ( typeof this.multiselectableFilterData[index]  !== 'undefined' ) {

                if ( Array.isArray( this.multiselectableFilterData[index] )) {
                  // array
                  if ( this.multiselectableFilterData[index].indexOf(row[element]) < 0 ) {
                    failedMemeters.push(element);
                  }
                } else {
                  if ( this.multiselectableFilterData[index] !== row[element] ) {
                    failedMemeters.push(element);
                  }
                }
              }
            });
          }

          if (failedMemeters.length > 0 ) {
            this.multiselectFailed.emit( {id: row.id, error: failedMemeters});
            return;
          }

        }
        // add or remove
        const idx =  this.highlightedRows.indexOf(row);
          if ( idx < 0 ) {
            this.highlightedRows.push(row);
          } else {
            this.highlightedRows.splice(idx, 1);
          }
      }
    }

    if ( !this.multiselectable) {
      this.rowClicked.emit(row.id);
    } else {
      const rowIds = [];
      const filterFilled = {};
      // fill actual values  from filtered members
      if (this.highlightedRows.length > 0 ) {
        this.multiselectableFilter.forEach(element => {
          filterFilled[element] = this.highlightedRows[0][element];
        });
      }
      this.highlightedRows.forEach(element =>  {
        rowIds.push( element.id);
      });
      this.rowClicked.emit( {filter: filterFilled, ids: rowIds});
    }
  }
}
